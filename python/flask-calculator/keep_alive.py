from flask import Flask
from threading import Thread
from os import environ

app = Flask("")

PORT = environ.get("APP_PORT", 3000)


def run():
    app.run(host="0.0.0.0", port=PORT)


def keep_alive():
    t = Thread(target=run)
    t.start()
